import { NgModule } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CrudService } from "./general-services/crud.service";
import { localStorageSync } from "ngrx-store-localstorage";
import { StoreModule, ActionReducer, MetaReducer } from "@ngrx/store";
import { AuthEffects } from './configurations/ngRx/effects/auth.effects';
import { reducers } from './configurations/ngRx/reducers/reducers';
import { AuthFacadeService } from './general-services/auth-facade.service';
import { HomeEffects } from './configurations/ngRx/effects/home.effects';
import { HomeFacadeService } from './general-services/home-facade.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return localStorageSync({
    keys: ["authData", "homeData"],
    rehydrate: true
  })(reducer);
}

const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([AuthEffects, HomeEffects])
  ],
  providers: [
    CrudService, 
    HomeFacadeService, 
    AuthFacadeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
