import { Component, OnInit } from "@angular/core";
import { CrudService } from "src/app/general-services/crud.service";
import { MatSnackBar } from "@angular/material";
import { SpecialFormGroup } from 'src/app/configurations/specialForms/SpecialForms';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';

@Component({
  selector: "entry-form",
  templateUrl: "./entry-form.component.html",
  styleUrls: ["./entry-form.component.scss"]
})
export class EntryFormComponent implements OnInit {
  form: SpecialFormGroup;
  constructor(private snackBar: MatSnackBar,
    private homeFacade: HomeFacadeService,
    private crud: CrudService) { }

  ngOnInit() {
    this.createForm();
    this.homeFacade.getAllUsers();
  }

  createForm() {
    // this.form = SpecialFormGenerator(getFormFields(masterTypes.users));
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000
    });
  }

  createUser() {
    if (this.form.valid) {
      const createdUser = Object.assign(this.form.value, {
        created: new Date().getTime(),
        userType: 1
      });
      this.homeFacade.createUser(createdUser)
      // this.crud.postLogin("auth/entry", createdUser).subscribe(response => {
      //   this.form.specialReset();
      //   this.openSnackBar("El usuario se creó correctamente:", "Creación");
      // });
    } else {
      this.form.unpristineRequired();
    }
  }
}
