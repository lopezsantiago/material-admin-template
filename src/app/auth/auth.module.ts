import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./login/login.component";
import { Routes, RouterModule } from "@angular/router";
import { CustomMaterialModule } from "../custom-material/custom-material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EntryFormComponent } from "./entry-form/entry-form.component";
import { BasicControlsModule } from '../common-controls/basic-controls.module';

export const masterRoutes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "entry",
    component: EntryFormComponent
  },
  { path: "**", redirectTo: "login" }
];

@NgModule({
  declarations: [LoginComponent, EntryFormComponent],
  imports: [
    BasicControlsModule,
    CustomMaterialModule,
    FormsModule,
    RouterModule.forChild(masterRoutes),
    CommonModule,
    ReactiveFormsModule
  ]
})
export class AuthModule {}
