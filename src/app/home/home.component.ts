import { Component, OnInit } from '@angular/core';
import { menuItem } from '../configurations/models/menuItem.module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-masters',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menuItems: menuItem[] = [
    {
      name: 'Fruit',
      icon: 'person',
      children: [
        {
          name: 'Apple',
          icon: 'person',
          action: () => {
            console.log('Hola')
          },
        },
        {
          name: 'Banana',
          icon: 'person'
        },
        {
          name: 'Fruit loops',
          icon: 'person'
        }
      ]
    },
    {
      name: 'Vegetables',
      icon: 'person',
      children: [
        {
          name: 'Green',
          icon: 'person',
          children: [
            {
              name: 'Broccoli',
              icon: 'person'
            },
            {
              name: 'Brussel sprouts',
              icon: 'person'
            },
          ]
        }, {
          name: 'Orange',
          icon: 'person',
          children: [
            {
              name: 'Pumpkins',
              icon: 'person',
            },
            {
              name: 'Carrots',
              icon: 'person',
              children: [
                {
                  name: 'Pumpkins',
                  icon: 'person',
                },
                {
                  name: 'Carrots',
                  icon: 'person',
                  children: [
                    {
                      name: 'Pumpkins',
                      icon: 'person',
                    },
                    {
                      name: 'Carrots',
                      icon: 'person',
                    },
                  ]
                },
              ]
            },
          ]
        },
      ]
    }
  ]

  navBarOptions: menuItem[] = [{
    route: ['home','pending'],
    icon: 'playlist_add_check'
  },
  {
    route: ['home','users'],
    icon: 'people'
  },
  {
    route: ['home','diets'],
    icon: 'local_dining'
  },
  {
    route: ['home','foods'],
    icon: 'fastfood'
  },
  {
    route: ['home','myinfo'],
    icon: 'person'
  },
  {
    route: ['home','usersconfig'],
    icon: 'group_add'
  },
  {
    route: ['home','news'],
    icon: 'autorenew'
  }];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  menuAction(menuItem: menuItem) {
    if (menuItem.action) {
      menuItem.action();
    }
    if (menuItem.route) {
      this.router.navigate(menuItem.route)
    }
  }
}
