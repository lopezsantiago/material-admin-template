import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersFormComponent } from './users-form/users-form.component';
import { Routes, RouterModule } from '@angular/router';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { PendingUsersComponent } from './pending-users/pending-users.component';
import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FoodsComponent } from './foods/foods.component';
import { DietsComponent } from './diets/diets.component';
import { NewsComponent } from './news/news.component';
import { BasicControlsModule } from '../common-controls/basic-controls.module';
import { SharedModule } from '../shared/shared.module';
import { ResolverService } from '../general-services/resolver.service';

const masterRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'news', component: NewsComponent },
      { path: 'users', component: UsersFormComponent },
      { path: 'foods', component: FoodsComponent, resolve: { items: ResolverService } },
      { path: 'diets', component: DietsComponent },
      { path: 'users/:uid', component: UsersFormComponent },
      { path: 'pending', component: PendingUsersComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ]
  }
];
@NgModule({
  declarations: [UsersFormComponent, PendingUsersComponent, HomeComponent, DashboardComponent, FoodsComponent, DietsComponent, NewsComponent],
  imports: [
    RouterModule.forChild(masterRoutes),
    CommonModule,
    CustomMaterialModule,
    ReactiveFormsModule,
    BasicControlsModule,
    SharedModule
  ],
  providers: [
    ResolverService
  ]
})
export class HomeModule { }
