import { Component, OnInit } from '@angular/core';
import { SpecialFormGroup } from '../../configurations/specialForms/SpecialForms';
import { SpecialFormGenerator, fieldDataToArray } from '../../configurations/specialForms/SpecialFormGenerator';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';
import { foodFields } from './foods.component.forms';

@Component({
  selector: 'foods',
  templateUrl: './foods.component.html',
  styleUrls: ['./foods.component.scss']
})
export class FoodsComponent implements OnInit {
  form: SpecialFormGroup;
  foods$ = this._homeFacade.foods$;
  formFields = [];
  constructor(private _homeFacade: HomeFacadeService) { }

  ngOnInit() {
    this.formFields = fieldDataToArray(foodFields(this._homeFacade));
    this.form = SpecialFormGenerator(this.formFields);
  }

  submit(value) {
    console.log('creado')
  }

}
