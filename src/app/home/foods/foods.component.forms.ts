import { controlTypes } from "src/app/configurations/specialForms/SpecialForms";
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';

export const foodFields = (homeFacade:HomeFacadeService) => Object.assign({
    categorie: {
      label: "Categoria",
      order: 1,
      data: [homeFacade.foodCategories$, 'id', 'name'],
      type: controlTypes.dropdown
    },
    group:
    {
      label: "Grupo",
      order: 2,
      data: [homeFacade.foodGroups$, 'id', 'name'],
      type: controlTypes.dropdown
    },
    name: {
      label: "Nombre",
      length: 100,
      order: 3,
      data: ["text"],
    },
    calories:
    {
      label: "Calorias",
      length: 20,
      order: 4,
      data: ["number"],
    },
    quantity:
    {
      label: "Cantidad",
      length: 20,
      order: 5,
      data: ["number"],
    },
    unity:
    {
      label: "Unidad",
      length: 100,
      order: 6,
      data: [homeFacade.measureUnits$, 'id', 'name'],
      type: controlTypes.dropdown
    }
  });