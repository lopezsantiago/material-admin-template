import { controlTypes } from 'src/app/configurations/specialForms/SpecialForms';

export const userFields=() => Object.assign({
    names: {
      label: "Nombres",
      length: 100,
      order: 1,
      data: ["text"],
    },
    lastNames: {
      label: "Apellidos",
      length: 100,
      order: 2,
      data: ["text"],
    },
    birthDate:
    {
      label: "Fecha nacimiento",
      order: 3,
      required: false,
      type: controlTypes.date
    },
    email: {
      label: "Correo",
      length: 100,
      order: 4,
      required: true,
      data: ["email"],
    },
    phone: {
      label: "Teléfono",
      length: 100,
      order: 5,
      required: false,
      data: ["tel"],
    },
    cellPhone: {
      label: "Celular",
      length: 100,
      order: 6,
      data: ["tel"],
    },
    document: {
      label: "Cédula",
      length: 20,
      order: 7,
      required: false,
      data: ["text"],
    },
    address: {
      label: "Dirección",
      length: 50,
      order: 8,
      required: false,
      data: ["text"],
    },
    sex: {
      label: "Sexo",
      length: 50,
      order: 9,
      required: false,
      data: ["text"],
    },
    accept: {
      label: "Acepto terminos y condiciones",
      order: 10,
      defect: false,
      type: controlTypes.boolean
    },
    city: {
      label: "Ciudad",
      length: 100,
      order: 11,
      required: false,
      data: ["text"],
    },
    // weight: {
    //   label: "Peso",
    //   order: 12,
    //   required: false,
    //   data: [this.getFormFields(masterTypes.weight)],
    //   type: controlTypes.array
    // },
    // history: {
    //   label: "Hisoria",
    //   order: 13,
    //   required: false,
    //   data: [this.getFormFields(masterTypes.weight)],
    //   type: controlTypes.array
    // },
    // diet: {
    //   label: "Dieta",
    //   order: 14,
    //   required: false,
    //   data: [this.getFormFields(masterTypes.weight)],
    //   type: controlTypes.array
    // },
    height: {
      label: "Altura",
      order: 15,
      required: false,
      data: ["text"],
    },
    profession: {
      label: "Profesión",
      order: 16,
      required: false,
      data: ["text"],
    },
    password: {
      label: "Contraseña",
      length: 100,
      order: 17,
      required: false,
      data: ["password"],
    }
  });