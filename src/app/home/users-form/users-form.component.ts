import { Component, OnInit } from "@angular/core";
import { SpecialFormGroup } from 'src/app/configurations/specialForms/SpecialForms';
import { SpecialFormGenerator, fieldDataToArray } from 'src/app/configurations/specialForms/SpecialFormGenerator';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';
import { userFields } from './users-form.component.forms';

@Component({
  selector: "app-users-form",
  templateUrl: "./users-form.component.html",
  styleUrls: ["./users-form.component.scss"]
})
export class UsersFormComponent implements OnInit {
  form: SpecialFormGroup;
  formFields=[];

  constructor(private _homeFacade: HomeFacadeService) { }

  ngOnInit() {
    this.createForm()
  }

  createForm() {
    this.formFields = fieldDataToArray(userFields());
    this.form = SpecialFormGenerator(this.formFields);
    console.log(this.form);
  }

  submit() {
    // this._crud.post("users", this.userData).subscribe(response => {
    //   this.dataSource = [...this.dataSource, response];
    // });
  }
}
