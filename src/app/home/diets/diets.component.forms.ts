import { HomeFacadeService } from 'src/app/general-services/home-facade.service';
import { controlTypes } from 'src/app/configurations/specialForms/SpecialForms';
import { Validators } from '@angular/forms';
import { fieldDataToArray } from 'src/app/configurations/specialForms/SpecialFormGenerator';

export const dietFields = (homeFacade:HomeFacadeService) => Object.assign({
    categorie: {
      label: "Categoria",
      data: [homeFacade.foodCategories$, 'id', 'name'],
      type: controlTypes.dropdown
    },
    onBillboard: {
      label: "en cartelera",
      type: controlTypes.boolean,
      validations:[Validators.requiredTrue]
    },
    description: {
      label: "Descripción",
      type: controlTypes.longText,
    },
    premiere: {
      label: "Fecha estreno",
      required:true,
      type: controlTypes.date
    },
    email: {
      label: "Correo",
      order: 1,
      required:true,
      length:30,
      validations:[Validators.email]
    },
    group:
    {
      label: "Grupo",
      data: [homeFacade.foodGroups$, 'id', 'name'],
      type: controlTypes.dropdown
    },
    name: {
      label: "Nombre",
      length: 100,
      data: ["text"],
    },
    duration:
    {
      label: "Duración (minutos)",
      length: 20,
      data: ["number"],
    },
    test:
    {
      label: "test autocomplete",
      length: 20,
      type:controlTypes.autocomplete,
      data: [
        (query) => './assets/mock-lists/mock.json',
        (data) => data.map(item => Object.assign({ name: item.nombre, id: item.nombre }))
      ],
    },
    director:
    {
      label: "Director",
      length: 20,
      type: controlTypes.form,
      data: [
        fieldDataToArray({
          name: {
            label: "Nombre",
            length: 100,
            order: 2,
            data: ["text"],
          },
          lastName: {
            label: "Apellido",
            length: 100,
            order: 3,
            data: ["text"],
          },
        })],
    },
    awards: {
      label: "Premios",
      length: 20,
      type: controlTypes.array,
      data: [
        fieldDataToArray({
          name: {
            label: "Nombre",
            length: 100,
            order: 2,
            data: ["text"],
          },
          lastName: {
            label: "Año",
            length: 4,
            order: 3,
            data: ["numeric"],
          },
        })],
    }
  });