import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { SpecialFormGenerator, fieldDataToArray } from 'src/app/configurations/specialForms/SpecialFormGenerator';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';
import { dietFields } from './diets.component.forms';

@Component({
  selector: 'app-diets',
  templateUrl: './diets.component.html',
  styleUrls: ['./diets.component.sass']
})
export class DietsComponent implements OnInit {
  form: FormGroup;
  formFields=[];
  constructor(private _homeFacade: HomeFacadeService) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formFields = fieldDataToArray(dietFields(this._homeFacade));
    this.form = SpecialFormGenerator(this.formFields);
  }

  submit(){
  }

}
