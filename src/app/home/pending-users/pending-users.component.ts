import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/general-services/crud.service';
import { UserModel } from 'src/app/configurations/models/users.model';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';

@Component({
  selector: 'app-pending-users',
  templateUrl: './pending-users.component.html',
  styleUrls: ['./pending-users.component.scss']
})
export class PendingUsersComponent implements OnInit {
  dataSource: Array<UserModel>;
  constructor(private homeFacade: HomeFacadeService) { }

  ngOnInit() {
    this.getUsers();
    this.homeFacade.users$.subscribe(data => {
      console.log(data);
      this.dataSource = data;
    })
  }

  getUsers() {
    this.homeFacade.getAllUsers();
  }

}
