import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'errorMessage'
})
export class ErrorMessagePipe implements PipeTransform {
  errorTypes = {
    maxlength: 'maxlengthError',
    required: 'requiredError',
    email: 'emailError',
    pattern:'patternError'
  }

  transform(errors: any): any {
    let error = '';
    Object.keys(errors).reverse().map(key => {
      error = this.errorTypes[key] || errors[key];
    });
    return error;
  }

}
