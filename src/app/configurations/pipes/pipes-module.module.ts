import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartAsyncPipe } from './smart-async/smart-async.pipe';
import { OrderArrayPipe } from './order-array/order-array.pipe';
import { KeysPipe } from './keys/keys.pipe';
import { ArrayFilterPipe } from './array-filter/array-filter.pipe';
import { ErrorMessagePipe } from './error-message/error-message.pipe';

const PIPES = [
  SmartAsyncPipe,
  OrderArrayPipe,
  KeysPipe,
  ArrayFilterPipe,
  ErrorMessagePipe
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...PIPES,
  ]
  , exports: [
    ...PIPES
  ]
})
export class PipesModule { }
