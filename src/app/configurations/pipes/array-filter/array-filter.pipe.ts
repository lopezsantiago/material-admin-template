import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'arrayFilter' })
export class ArrayFilterPipe implements PipeTransform {
  transform(resources: Array<Object>, fieldName: string, valueName: string, type = true): Array<Object> {
    let res = [];
    if (type)
      res = resources ? resources.filter(resource => resource[fieldName] == valueName) : [];
    else
      res = resources ? resources.filter(resource => resource[fieldName] != valueName) : [];
    return res;
  }
}