import { Pipe, PipeTransform } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Pipe({
  name: 'smartAsync'
})
export class SmartAsyncPipe implements PipeTransform {

  transform(value: any): any {
    if (value instanceof Observable)
      return value;
    else {
      return new BehaviorSubject(value);
    }
  }

}
