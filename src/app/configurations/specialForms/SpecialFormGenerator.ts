import { Validators } from "@angular/forms";
import {
  SpecialFormGroup,
  SpecialFormControl,
  FieldDescriptor,
  controlTypes,
  SpecialFormArray
} from "./SpecialForms";

export const SpecialFormGenerator = (
  fields: FieldDescriptor[]
): SpecialFormGroup => {
  const form = new SpecialFormGroup({});
  fields.forEach(field => {
    let validations: Array<any> = [];
    if (field.type === controlTypes.array) {
      if (field.data[0] instanceof Array) {
        const auxForm = SpecialFormGenerator(field.data[0]);
        form.addControl(field.name,
          new SpecialFormArray(field, auxForm, [], field.required ? arrayLengthRequired : undefined));
      }
    } else if (field.type === controlTypes.form) {
      if (field.data[0] instanceof Array) {
        const auxForm = SpecialFormGenerator(field.data[0]);
        form.addControl(field.name, auxForm);
      }
    } else {
      if (field.required) validations.push(Validators.required);
      if (field.length) validations.push(Validators.maxLength(field.length));
      validations = validations.concat(field.validations);
      form.addControl(
        field.name,
        new SpecialFormControl(field, field.defect, validations)
      );
    }
  });
  return form;
}
export const fieldDataToArray = (fields: Object): FieldDescriptor[] =>
  Object.keys(fields).map(name =>
    setDefectFieldOptions(Object.assign({ "name": name }, fields[name])));

export const setDefectFieldOptions = (field: FieldDescriptor): FieldDescriptor =>
  Object.assign({
    label: "",
    length: 0,
    order: 0,
    name: name,
    defect: "",
    required: true,
    readOnly: false,
    data: [],
    validations:[],
    type: controlTypes.text
  }, field);



export const dateFormat = (dateTime: Date) => {
  const date = dateTime.toISOString().split("T")[0];
  const time = dateTime.toTimeString().split(" ")[0];
  return date + " " + time;
};

export const arrayLengthRequired = aux => {
  if (aux.value.length) {
    return null;
  }
  return { noItems: "no hay ningun item seleccionado" };
};
