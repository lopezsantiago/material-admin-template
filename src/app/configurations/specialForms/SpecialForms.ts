import {
  FormControl,
  AsyncValidatorFn,
  ValidatorFn,
  FormGroup,
  AbstractControl,
  Validators,
  FormArray
} from "@angular/forms";
import * as _ from "lodash";

export enum controlTypes {
  text = 'text',
  pkey = 'pkey',
  longText = 'longText',
  dropdown = 'dropdown',
  date = 'date',
  time = 'time',
  boolean = 'boolean',
  image = 'image',
  autocomplete = 'autocomplete',
  array = 'array',
  numeric = 'numeric',
  form = 'form'
}
export interface FieldDescriptor {
  idType?: string;
  label?: string;
  length?: number;
  order?: number;
  name?: string;
  defect?: any;
  required?: boolean;
  readOnly?: boolean;
  validations?:Array<Function>;
  data?: any;
  type?:
  | controlTypes.pkey
  | controlTypes.text
  | controlTypes.longText
  | controlTypes.dropdown
  | controlTypes.date
  | controlTypes.time
  | controlTypes.boolean
  | controlTypes.image
  | controlTypes.array
  | controlTypes.form
  | controlTypes.autocomplete
  | controlTypes.numeric
}

export class SpecialFormControl extends FormControl {
  public label: string = "";
  public name: string = "";
  public tooltip: string = "";
  public defect: any = "";
  public order: number = 0;
  public data: any[] = [];
  public type: string = "text";
  public readOnly = false;
  public required = false;
  public validatorsBuffer: ValidatorFn | ValidatorFn[] | null;

  constructor(
    configObject: FieldDescriptor,
    formState?: any,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
  ) {
    super(formState, validatorOrOpts, asyncValidator);
    this.defect = formState;
    Object.assign(this, configObject);
    this.validatorsBuffer = validatorOrOpts;
  }

  //Setea si el control es de solo lectura
  setReadOnly(status: boolean) {
    this.readOnly = status;
  }

  /**
   * Método que permite agregar una validacion sin perder las anteriores
   * @param Validation
   */

  setSpecialValidation(Validation: ValidatorFn) {
    if (this.validatorsBuffer instanceof Array) {
      let requiredIndex = this.validatorsBuffer.findIndex(
        item => item === Validation
      );
      if (requiredIndex == -1) {
        this.validatorsBuffer.push(Validators.required);
        this.setValidators(this.validatorsBuffer);
      }
    }
  }

  changeData(newData: string[]) {
    this.data = newData;
  }

  /**
   * Método que permite eliminar una de las validaciones que se tiene.
   * @param Validation
   */

  deleteSpecialValidation(Validation: ValidatorFn) {
    if (this.validatorsBuffer instanceof Array) {
      let requiredIndex = this.validatorsBuffer.findIndex(
        item => item === Validation
      );
      if (requiredIndex != -1) {
        this.validatorsBuffer.splice(requiredIndex, 1);
        this.setValidators(this.validatorsBuffer);
      }
    }
  }
  //setea el requerido al control

  setRequired(status: boolean) {
    this.required = status;
    if (this.validatorsBuffer instanceof Array) {
      let requiredIndex = this.validatorsBuffer.findIndex(
        item => item === Validators.required
      );
      if (requiredIndex == -1 && status) {
        this.validatorsBuffer.push(Validators.required);
        this.setValidators(this.validatorsBuffer);
      } else if (requiredIndex != -1 && !status) {
        this.validatorsBuffer.splice(requiredIndex, 1);
        this.setValidators(this.validatorsBuffer);
      }
    }
  }

  specialReset() {
    this.setValue(this.defect);
  }
}

export class SpecialFormGroup extends FormGroup {
  constructor(
    controls: {
      [key: string]: SpecialFormControl;
    },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null,
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  unpristineRequired() {
    let auxReset = {};
    for (let key in this.controls) {
      if (this.controls[key] instanceof SpecialFormControl) {
        if (!(<SpecialFormControl>this.controls[key]).valid) {
          (<SpecialFormControl>this.controls[key]).markAsDirty();
        }
      } else if (this.controls[key] instanceof SpecialFormArray) {
        (<SpecialFormArray>this.controls[key]).unpristineRequired();
      }
    }
  }

  specialReset() {
    let auxReset = {};
    for (let key in this.controls) {
      if (this.controls[key] instanceof SpecialFormControl) {
        auxReset[key] = (<SpecialFormControl>this.controls[key]).defect;
      } else if (this.controls[key] instanceof SpecialFormArray) {
        (<SpecialFormArray>this.controls[key]).specialDelete();
      }
    }
    this.reset(auxReset);
  }

  /**
   * donde value es el objeto semilla y detailForm es un arreglo de form key para dar un marco al formarray
   */

  specialResetWithValue(value) {
    this.reset(value);
    for (let key in this.controls) {
      if (this.controls[key] instanceof SpecialFormArray) {
        (<SpecialFormArray>this.controls[key]).fillFormArray(value[key]);
      }
    }
  }

  getIdPkey(): SpecialFormControl {
    for (let key in this.controls) {
      if (this.controls[key] instanceof SpecialFormControl) {
        if ((<SpecialFormControl>this.controls[key]).type == "pkey") {
          return <SpecialFormControl>this.controls[key];
        }
      }
    }
    return null;
  }
}

export class SpecialFormArray extends FormArray {
  public label: string = "";
  public tooltip: string = "";
  public data: string[];
  public readOnly = false;
  public form: SpecialFormGroup;
  public required = false;
  public validatorsBuffer: ValidatorFn | ValidatorFn[] | null;
  constructor(
    configObject: FieldDescriptor,
    form: SpecialFormGroup,
    controls: AbstractControl[],
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
  ) {
    super(controls, validatorOrOpts, asyncValidator);
    this.label = configObject.label;
    this.form = form;
    this.required = configObject.required;
    this.validatorsBuffer = validatorOrOpts;
  }

  specialDelete() {
    let length = this.controls.length;
    for (let i = 0; i < length; i++) {
      this.removeAt(0);
    }
  }

  fillFormArray(data: Array<Object>) {
    this.specialDelete();
    data.forEach(item => {
      this.form.specialResetWithValue(item);
      this.push(_.cloneDeep(this.form));
    });
    this.form.reset();
  }

  SpecialPush(index?) {
    if (index == null || index == undefined) this.push(_.cloneDeep(this.form));
    else this.controls[index].reset(this.form.value);
  }

  editControl(index: number) {
    this.form.reset(this.controls[index].value);
  }

  unpristineRequired() {
    this.markAsDirty();
    this.controls.forEach(item => {
      if (item instanceof SpecialFormGroup) {
      }
      (<SpecialFormGroup>item).unpristineRequired();
    });
  }

  // recursiveFillForm(form: FormGroup, data: Object): FormGroup {
  //     for (let key in form.controls) {
  //         let control = form.controls[key];
  //         if (control instanceof FormControl) {
  //             control.setValue(data[key])
  //         } else if (control instanceof FormArray) {
  //             data[key].forEach(element => {

  //             });
  //         } else if (control instanceof FormGroup) {
  //             control.reset(data[key])
  //         }
  //     }
  //     return form;
  // }
}
