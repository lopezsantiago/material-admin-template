# Formularios especiales
La siguiente es una implementación que permite hacer uso de las clases de forms de angular con algunos métodos y propiedades que se han venido necesitando a lo largo del tiempo y la experiencia que se va consiguiento con cada proyecto realizado. 

## Empezando

Es necesario entender como funcionan los formularios reactivos de angular, formControl, formGroup, formArray y cada una de sus propiedades. Aqui un enlace para estudiar: https://angular.io/guide/reactive-forms

### app-form.service.ts
Este servicio se instancia al iniciar la aplicación y contiene los formularios de cada una de las maestras de la aplicación. Contiene un método que mediante el identificador de cada una de las maestras retorna el objeto con el que se crea cada formulario. Este servicio también tiene como inyección el facade de la aplicación. Lo cual le permite retorna los selectores (Observables donde se puede tener acceso a un elemento en el store).

### FormField

El formfield retorna todas las posibles opciones de un campo para crear cada control que debe ser diligenciado de forma correcta para que el formulario pueda ser valido.

##### Propiedades.

FieldDescriptor {
  label?: Campo de texto de la etiqueta o titulo del campo ;
  length?: valor que permite poner la validacion de un largo máximo;
  order?: valor que permite ubicarlo en un orden específico (Tabla);
  name?: Campo de texto para guardar el nombre o como se quiere enviar;
  defect?: Es el valor que se quiere que tenga cuando se reinicie el formulario;
  required?: boolean-Campo que habilita la validación de requerido;
  readOnly?: boolean-Campo que permite saber si es un campo de solo lectura
  validations?:Es un arreglo de validaciones personalizadas para el campo.
  data?: any-Es un campo especial que permite guardar información dependiendo del tipo de campo (normalmente se guarda en forma de arreglo)
  type?:
  | controlTypes.pkey :campo de tipo primary key, es un campo que en cuanto a formulario no cumple ninguna función, pero es necesario enviarlo a base de datos.
  ----------------------------------------------------------------------------
  | controlTypes.text: Es un campo de texto básico, en el campo data es necesario ubicar el tipo de texto, Ej data:['text'];
  ----------------------------------------------------------------------------
  | controlTypes.longText: Es un campo de texto básico pero habilita en vez de un input, un text area.
  -----------------------------------------------------------------------------
  | controlTypes.dropdown: Es un campo de selección básico. En el campo data se hace necesario ubicar la siguiente informacion:
  data: [Fuente de datos<Array|Obeservable<Array>>, nombre del campo de valor, nombre del campo de texto],
  ------------------------------------------------------------------------------
  | controlTypes.date: Es un campo de selección de fecha básico.
  ------------------------------------------------------------------------------
  | controlTypes.time: Es un campo de selección de horas básico.
  ------------------------------------------------------------------------------
  | controlTypes.boolean: Campo de selección true o false.
  Como opcional
  ------------------------------------------------------------------------------
  | controlTypes.image
  ---------------------------------------------------------------------------
  | controlTypes.array: Campo que genera un formArray, estos cumplen la función de agrupar elementos de la misma clase, por ejemplo una lista de permisos.
  data: Este tipo de dato espera en el primer slot de data otro objeto que describa un formulario.
  ------------------------------------------------------------------------------
  | controlTypes.form: Campo que permite agrupar formularios dentrode otro formulario. Esto permite una correcta separación de las partes que conforman un formulario.
   data: Este tipo de dato espera en el primer slot de data otro objeto que describa un formulario.
   -----------------------------------------------------------------------------
  | controlTypes.autocomplete:Campo que permite generar un campo de autocomplete. Este campo permite hacer un llamado a un servicio de búsqueda en el cual mediante un texto o una pequeña parte de la busqueda se traigan algunos posibles resultados y escoger uno de ellos.
  data: Este campo espera 2 metodos que permiten configurar la busqueda.
  (input value)=>return query url
  (data from service)=>formatear opcion en arreglo de objetos:value-name.
  -----------------------------------------------------------------------------

  | controlTypes.numeric

}