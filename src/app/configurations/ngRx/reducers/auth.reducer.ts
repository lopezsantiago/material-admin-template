import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as authActions from '../actions/auth.actions';
import { AuthDataState } from './app.states';

const initialState: AuthDataState =
{
    token: '',
    currentUser: null
};

export function reducer(state = initialState, action: authActions.All): AuthDataState {
    switch (action.type) {
        case authActions.AuthActionsNames.LOGIN_SUCCESS: {
            return Object.assign({}, state, { mastersFields: action.payload });
        }
        default: {
            return state;
        }
    }
}

export const getAuthState = createFeatureSelector<AuthDataState>('authData');

export const currentUserSelector = createSelector(getAuthState, (state: AuthDataState) => state.currentUser);
export const tokenSelector = createSelector(getAuthState, (state: AuthDataState) => state.token);
