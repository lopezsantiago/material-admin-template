import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';

import * as authReducer from './auth.reducer';
import * as homeReducer from './home.reducer';

import { AppState } from "./app.states";

export const reducers: ActionReducerMap<AppState> = {
    authData: authReducer.reducer,
    homeData: homeReducer.reducer
};
