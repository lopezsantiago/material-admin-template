import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as homeActions from '../actions/home.actions';
import { HomeDataState } from './app.states';

const initialState: HomeDataState =
{
    pendingUsers: [],
    users: [],
    foods: [],
    foodCategories: [],
    foodGroups: [],
    masureUnits: []

};

export function reducer(state = initialState, action: homeActions.All): HomeDataState {
    switch (action.type) {
        //--USERS--------------------------------------------------------
        case homeActions.HomeActionsNames.GETALLUSERS: {
            return Object.assign({}, state, { users: action.payload });
        }
        case homeActions.HomeActionsNames.CREATEUSER: {
            return Object.assign({}, state, { users: [[...state.users], action.payload] });
        }


        //--FOODS----------------------------------------------------------
        case homeActions.HomeActionsNames.GETALLFOODS: {
            return Object.assign({}, state, { foods: action.payload });
        }
        case homeActions.HomeActionsNames.CREATEFOOD: {
            return Object.assign({}, state, { foods: [[...state.foods], action.payload] });
        }


        //---------------General masters--------------------------------
        case homeActions.HomeActionsNames.GETALLFOODCATEGORIES: {
            return Object.assign({}, state, { foodCategories: action.payload });
        }
        case homeActions.HomeActionsNames.GETALLFOODGROUPS: {
            return Object.assign({}, state, { foodGroups: action.payload });
        }
        case homeActions.HomeActionsNames.GETALLMEASUREUNITS: {
            return Object.assign({}, state, { masureUnits: action.payload });
        }
        default: {
            return state;
        }
    }
}

export const getHomeState = createFeatureSelector<HomeDataState>('homeData');

export const HomeGeneralSelectors = {
    UsersSelector: createSelector(getHomeState, (state: HomeDataState) => state.users),
    FoodSelector: createSelector(getHomeState, (state: HomeDataState) => state.foods),
    FoodCategoriesSelector: createSelector(getHomeState, (state: HomeDataState) => state.foodCategories),
    FoodGroupsSelector: createSelector(getHomeState, (state: HomeDataState) => state.foodGroups),
    MeasureUnitsSelector: createSelector(getHomeState, (state: HomeDataState) => state.masureUnits),
}