import { UserModel } from '../../models/users.model';
import { BasicMasterModel } from '../../models/basicMaster.model';
import { FoodModel } from '../../models/food.model';

/**
 * Lugar donde se definen los estados de la aplicación
 */

export interface AppState {
    authData: AuthDataState,
    homeData: HomeDataState
}

/**
 * 
 */

export interface AuthDataState {
    token: string,
    currentUser: UserModel,
}

export interface HomeDataState {
    users: UserModel[],
    foods:FoodModel[],
    pendingUsers: UserModel[],
    foodCategories:BasicMasterModel[],
    foodGroups:BasicMasterModel[],
    masureUnits:BasicMasterModel[],

}
