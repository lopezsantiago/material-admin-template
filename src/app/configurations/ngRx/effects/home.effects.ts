import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { CrudService } from 'src/app/general-services/crud.service';
import * as homeActions from '../actions/home.actions';
import { requestDataModel } from '../../models/requestData.model'
@Injectable()
export class HomeEffects {

    constructor(
        private _crud: CrudService,
        private actions$: Actions) { }

    // Listen for the 'create' action
    @Effect()
    getRequest$: Observable<Action> = this.actions$.pipe(
        ofType<homeActions.GetRequestAction>(homeActions.HomeActionsNames.GETREQUEST),
        map(action => action.payload),
        mergeMap((requestData: requestDataModel) => {
            return this._crud.get(requestData.uri).pipe(
                map((data: any) => {
                    return new requestData.success(data);
                }),
                catchError(() => {
                    return of({ type: 'LOGIN_FAILED' })
                })
            )
        })
    );

    @Effect()
    createRequest$: Observable<Action> = this.actions$.pipe(
        ofType<homeActions.CreateRequestAction>(homeActions.HomeActionsNames.CREATEREQUEST),
        map(action => action.payload),
        mergeMap((requestData: requestDataModel) => {
            return this._crud.post(requestData.uri, requestData.body).pipe(
                map((data: any) => {
                    return new requestData.success(data);
                }),
                catchError(() => {
                    return of({ type: 'LOGIN_FAILED' })
                })
            )
        })
    );


    @Effect()
    updateRequest$: Observable<Action> = this.actions$.pipe(
        ofType<homeActions.GetRequestAction>(homeActions.HomeActionsNames.UPDATEREQUEST),
        map(action => action.payload),
        mergeMap((requestData: requestDataModel) => {
            return this._crud.get(requestData.uri).pipe(
                map((data: any) => {
                    return new requestData.success(data);
                }),
                catchError(() => {
                    return of({ type: 'LOGIN_FAILED' })
                })
            )
        })
    );
}


