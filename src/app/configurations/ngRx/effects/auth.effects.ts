import { AppState } from './../reducers/app.states';
import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { CrudService } from 'src/app/general-services/crud.service';
import * as authActions from '../actions/auth.actions';
import { loginResponseModel } from '../../models/loginResponse.model';
import { LoginModel } from '../../models/login.model';
import { appUris } from '../../General-configs';

@Injectable()
export class AuthEffects {

    constructor(
        private store: Store<AppState>,
        private _crud: CrudService,
        private actions$: Actions) { }

    // Listen for the 'LOGIN' action
    @Effect()
    login$: Observable<Action> = this.actions$.pipe(
        ofType<authActions.doLoginAction>(authActions.AuthActionsNames.LOGIN),
        map(action => action.payload),
        mergeMap((loginCredentials: LoginModel) => {
            return this._crud.postLogin(appUris.login, JSON.stringify(loginCredentials)).pipe(
                map((data: loginResponseModel) => {
                    return new authActions.loginSuccessAction(data)
                }),
                catchError(() => {
                    return of({ type: 'LOGIN_FAILED' })
                })
            )
        })
    );
}


