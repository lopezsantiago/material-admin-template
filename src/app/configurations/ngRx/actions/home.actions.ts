import { Action } from '@ngrx/store';
import { UserModel } from '../../models/users.model';
import { requestDataModel } from '../../models/requestData.model';
import { BasicMasterModel } from '../../models/basicMaster.model';
import { FoodModel } from '../../models/food.model';

export const HomeActionsNames = {
    GETREQUEST: '[HOME] get request action',
    CREATEREQUEST: '[HOME] create request action',
    UPDATEREQUEST: '[HOME] update request action',
    DELETEREQUEST: '[HOME] delete request action',

    GETPENDINGUSERS: '[HOME] get pending users action',
    GETALLUSERS: '[HOME] Get all users action',
    CREATEUSER: '[HOME] create user action',
    UPDATEUSER: '[HOME] update user action',
    DELETEUSER: '[HOME] delete user action',

    GETALLFOODCATEGORIES: '[HOME] Get all food categories',
    GETALLFOODGROUPS: '[HOME] Get all food groups',
    GETALLMEASUREUNITS: '[HOME] Get all measure inits',


    GETALLFOODS: '[HOME] Get all foods action',
    CREATEFOOD: '[HOME] create food action',
    UPDATEFOOD: '[HOME] update food action',
    DELETEFOODR: '[HOME] delete food action',
}

//acción que se encarga de hacer la petición para ssaber si las credenciales
//ingresadas son correctas
//----REQUEST--------------------------------------------
//Get all
export class GetRequestAction implements Action {
    readonly type = HomeActionsNames.GETREQUEST;
    constructor(public payload: requestDataModel) { }
}
// Create
export class CreateRequestAction implements Action {
    readonly type = HomeActionsNames.CREATEREQUEST;
    constructor(public payload: requestDataModel) { }
}
// Update
export class UpdateRequestAction implements Action {
    readonly type = HomeActionsNames.UPDATEUSER;
    constructor(public payload: requestDataModel) { }
}
//------------------------------------------------------------------------------
export class GetAllUsersAction implements Action {
    readonly type = HomeActionsNames.GETALLUSERS;
    constructor(public payload: UserModel[]) { }
}

export class CreateUsersAction implements Action {
    readonly type = HomeActionsNames.CREATEUSER;
    constructor(public payload: UserModel) { }
}

export class UpdateUsersAction implements Action {
    readonly type = HomeActionsNames.UPDATEUSER;
    constructor(public payload: UserModel) { }
}
//----FOODS-.--------------------------------------
export class GetAllFoodsAction implements Action {
    readonly type = HomeActionsNames.GETALLFOODS;
    constructor(public payload: FoodModel[]) { }
}

export class CreateFoodsAction implements Action {
    readonly type = HomeActionsNames.CREATEFOOD;
    constructor(public payload: FoodModel) { }
}

export class UpdateFoodsAction implements Action {
    readonly type = HomeActionsNames.UPDATEFOOD;
    constructor(public payload: FoodModel) { }
}



//--------------------------------------------------------------------------
//---Basic masters
export class GetAllFoodCategoriesAction implements Action {
    readonly type = HomeActionsNames.GETALLFOODCATEGORIES;
    constructor(public payload: BasicMasterModel[]) { }
}
export class GetAllFoodGroupsAction implements Action {
    readonly type = HomeActionsNames.GETALLFOODGROUPS;
    constructor(public payload: BasicMasterModel[]) { }
}

export class GetAllMeasureUnitsAction implements Action {
    readonly type = HomeActionsNames.GETALLMEASUREUNITS;
    constructor(public payload: BasicMasterModel[]) { }
}



export type All =
    GetRequestAction |
    CreateRequestAction |
    UpdateRequestAction |

    GetAllUsersAction |
    CreateUsersAction |
    UpdateUsersAction |

    GetAllFoodsAction |
    CreateFoodsAction |
    UpdateFoodsAction |

    GetAllFoodCategoriesAction |
    GetAllFoodGroupsAction |
    GetAllMeasureUnitsAction;