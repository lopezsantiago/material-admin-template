import { Action } from '@ngrx/store';
import { LoginModel } from '../../models/login.model';
import { loginResponseModel } from '../../models/loginResponse.model';

export const AuthActionsNames={
    LOGIN:'[AUTH] Login action',
    LOGIN_SUCCESS:'[AUTH] Login success action',
    CUSTOMER_REQUEST:'[AUTH] Customer creation request'
}

//acción que se encarga de hacer la petición para ssaber si las credenciales
//ingresadas son correctas
export class doLoginAction implements Action {
    readonly type = AuthActionsNames.LOGIN;
    constructor(public payload: LoginModel) { }
}
//accion que se en
export class loginSuccessAction implements Action {
    readonly type = AuthActionsNames.LOGIN_SUCCESS;
    constructor(public payload: loginResponseModel) { }
}

export type All =
    doLoginAction |
    loginSuccessAction
