import { UserModel } from './users.model';

export interface loginResponseModel {
    user:UserModel,
    token:string
}