export interface eventModel{
    html:string,
    name:string,
    gallery:string[],
    date:number
}