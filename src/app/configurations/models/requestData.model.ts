import { Action } from '@ngrx/store';

export interface requestDataModel {
    uri: string,
    success: any,
    body?: Object,
}