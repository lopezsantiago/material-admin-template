export interface UserModel {
  names: String;
  lastNames: String;
  birthDate: Number;
  email: String;
  phone: String;
  cellPhone: String;
  document: Number;
  created: Number;
  address: String;
  sex: String;
  accept: Boolean;
  city: String;
  weight: Array<any>;
  history: Array<any>;
  height: Number;
  diet: Array<any>;
  profession: String;
  userType: Number;
  password: String;
  userPhoto: String;
  status: Boolean;
}
