import { BasicMasterModel } from './basicMaster.model';

export interface FoodModel{
    categorie:BasicMasterModel,
    group:BasicMasterModel,
    name:string,
    calories:number,
    unity:BasicMasterModel,
    quantity:number
}