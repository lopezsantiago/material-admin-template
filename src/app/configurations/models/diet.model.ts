import { FoodModel } from './food.model';

export interface DietModel {
    name: string;
    description: string;
    foods: FoodModel[];
}