export interface menuItem{
    name?:string;
    route?:string[];
    icon?:string;
    children?:Array<menuItem>;
    action?:Function;
}