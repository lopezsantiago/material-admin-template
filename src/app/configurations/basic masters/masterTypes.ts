export enum masterTypes {
    users = "fields of the general user form",
    foods = "Foods",
    weight = 'weight',
    foodCategories = 'Food categories master',
    foodGroups = 'Food groups master',
    measureUnits = 'measure units master',
    movies='movies'
  }
  