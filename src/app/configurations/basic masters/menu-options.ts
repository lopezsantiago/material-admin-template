export const MenuOptions = [
    {
        route: 'pending',
        icon: 'playlist_add_check',
        permitedUsers: [3]
    },
    {
        route: 'users',
        icon: 'people',
        permitedUsers: [3]
    },
    {
        route: 'diets',
        icon: 'local_dining',
        permitedUsers: [2, 3]
    },
    {
        route: 'foods',
        icon: 'fastfood',
        permitedUsers: [2, 3]
    },
    {
        route: 'myinfo',
        icon: 'person',
        permitedUsers: [2]
    },
    {
        route: 'usersconfig',
        icon: 'group_add',
        permitedUsers: [3]
    },
    {
        route: 'news',
        icon: 'autorenew',
        permitedUsers: [3]
    }
]