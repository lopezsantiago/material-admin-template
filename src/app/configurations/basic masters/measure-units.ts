import { BasicMasterModel } from '../models/basicMaster.model';

export const measureUnits:BasicMasterModel[] = [
    { id: '1', name: "mililitro" },
    { id: '2', name: 'onza' }
];
