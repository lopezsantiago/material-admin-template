import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatTreeModule,
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatSidenavModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatIconModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSortModule,
} from '@angular/material';

const MATERIAL_MODULE = [
  MatTreeModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatInputModule,
  MatSlideToggleModule,
  MatDialogModule,
  MatSidenavModule,
  MatStepperModule,
  MatSnackBarModule,
  MatTableModule,
  MatExpansionModule,
  MatIconModule,
  MatSelectModule,
  MatPaginatorModule,
  MatSortModule 
]

@NgModule({
  imports: [
    CommonModule,
    ...MATERIAL_MODULE
  ],
  exports: [
    ...MATERIAL_MODULE
  ],
  declarations: []
})
export class CustomMaterialModule { }
