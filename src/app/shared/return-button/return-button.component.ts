import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'return-button',
  templateUrl: './return-button.component.html',
  styleUrls: ['./return-button.component.scss']
})
export class ReturnButtonComponent implements OnInit {
  @Input('show') show = true;
  @Output('click') click: EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  buttonAction() {

  }
}
