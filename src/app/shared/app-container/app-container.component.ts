import {
  Component,
  Input,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';
import { MatDrawer, MatTreeNestedDataSource } from '@angular/material';
import { MenuOptions } from 'src/app/configurations/basic masters/menu-options';
import { menuItem } from 'src/app/configurations/models/menuItem.module';
import { NestedTreeControl } from '@angular/cdk/tree';

@Component({
  selector: 'app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.scss'],
  host: {
    '(document:click)': 'onClick($event)',
    '(window:resize)': 'onResize($event)'
  },
})
export class AppContainerComponent {
  @Input('menuItems') menuItems: menuItem[] = [];
  @Input('navBarOptions') navBarOptions: menuItem[] = [];

  @Output('optionClick') optionClick: EventEmitter<any> = new EventEmitter();
  @ViewChild('drawer', { static: true }) _drawer: MatDrawer;
  drawerType = '';

  treeControl = new NestedTreeControl<menuItem>(node => node.children);
  dataSource = new MatTreeNestedDataSource<menuItem>();

  onResize() {
    if (window.innerWidth < 768) {
      this.drawerType = 'over';
    } else {
      this.drawerType = 'side';
    }
  }

  onClick() {
    if (this._drawer && this._drawer.opened) {
      this._drawer.close();
    }
  }

  constructor() {
  }

  hasChild = (_: number, node: menuItem) => !!node.children && node.children.length > 0;

  ngOnInit() {
    this.onResize();
    this.dataSource.data = this.menuItems;
  }

  openDrawer() {
    this._drawer.toggle();
  }
}
