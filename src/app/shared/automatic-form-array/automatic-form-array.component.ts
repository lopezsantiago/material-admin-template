import { Component, OnInit, Input } from '@angular/core';
import { SpecialFormArray } from 'src/app/configurations/specialForms/SpecialForms';

@Component({
  selector: 'automatic-form-array',
  templateUrl: './automatic-form-array.component.html',
  styleUrls: ['./automatic-form-array.component.scss']
})
export class AutomaticFormArrayComponent implements OnInit {
  @Input('formArray') formArray: SpecialFormArray;

  constructor() { }

  ngOnInit() {
  }

  submit(){
    this.formArray.SpecialPush();
    this.formArray.form.specialReset();
  }

  delete(index){
    this.formArray.removeAt(index);
  }
}
