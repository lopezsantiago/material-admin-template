import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticFormArrayComponent } from './automatic-form-array.component';

describe('AutomaticFormArrayComponent', () => {
  let component: AutomaticFormArrayComponent;
  let fixture: ComponentFixture<AutomaticFormArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomaticFormArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticFormArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
