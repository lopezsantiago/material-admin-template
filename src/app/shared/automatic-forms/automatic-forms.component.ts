import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpecialFormGroup, controlTypes } from 'src/app/configurations/specialForms/SpecialForms';

@Component({
  selector: 'automatic-forms',
  templateUrl: './automatic-forms.component.html',
  styleUrls: ['./automatic-forms.component.scss']
})
export class AutomaticFormsComponent implements OnInit {
  @Input('form') form: SpecialFormGroup;
  @Input('cancelText') cancelText: string = 'cancel';
  @Input('successText') successText: string = 'success';
  @Input('enableButtons') enableButtons: boolean = true;

  @Output('submit') submit: EventEmitter<any> = new EventEmitter();
  @Output('cancel') cancel: EventEmitter<any> = new EventEmitter();

  controlTypes = controlTypes;
  constructor() { }

  ngOnInit() { }

  submitAction() {
    this.form.valid ?
      this.submit.emit(this.form.value) :
      this.form.unpristineRequired();
  }

  cancelAction(){
    this.cancel.emit();
    this.form.specialReset()
  }

}
