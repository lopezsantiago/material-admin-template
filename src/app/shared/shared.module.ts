import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../configurations/pipes/pipes-module.module';
import { AutomaticFormsComponent } from './automatic-forms/automatic-forms.component';
import { BasicControlsModule } from '../common-controls/basic-controls.module';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ListTableComponent, getCellDataPipe, orderTableColumns } from './list-table/list-table.component';
import { ReturnButtonComponent } from './return-button/return-button.component';
import { AppContainerComponent } from './app-container/app-container.component';
import { RouterModule } from '@angular/router';
import { AutomaticFormArrayComponent } from './automatic-form-array/automatic-form-array.component';


const components = [
  AutomaticFormsComponent,
  ListTableComponent,
  getCellDataPipe,
  orderTableColumns,
  ReturnButtonComponent,
  AppContainerComponent,
  AutomaticFormArrayComponent
]
@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    PipesModule,
    BasicControlsModule,
    CustomMaterialModule,
    RouterModule
  ],
  exports: [...components]
})
export class SharedModule { }
