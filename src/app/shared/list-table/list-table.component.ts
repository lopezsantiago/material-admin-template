import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
import { controlTypes, FieldDescriptor } from 'src/app/configurations/specialForms/SpecialForms';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Pipe({ name: 'orderTableColumns' })
export class orderTableColumns implements PipeTransform {
  transform(data: FieldDescriptor[]): any {
    return data.map(item => item.name);
  }
}

@Pipe({ name: 'getCellData' })
export class getCellDataPipe implements PipeTransform {
  transform(data: Object, fieldData: FieldDescriptor): any {
    switch (fieldData.type) {
      case controlTypes.text: return data[fieldData.name];
      case controlTypes.dropdown: return data[fieldData.name][fieldData.data[2]];
    }
  }
}

@Component({
  selector: 'list-table',
  templateUrl: './list-table.component.html',
  styleUrls: ['./list-table.component.scss'],
})
export class ListTableComponent implements OnInit {
  @Input('data') data: any[];
  @Input('structure') structure: Object;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  controlTypes = controlTypes;
  dataSource: MatTableDataSource<any>;
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.data && changes.data.currentValue) {
      this.dataSource = new MatTableDataSource<any>(this.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnInit() {
  }

}


