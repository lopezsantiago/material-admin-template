import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SpecialFormControl } from '../../configurations/specialForms/SpecialForms';

@Component({
  selector: 'control-input',
  templateUrl: './control-input.component.html',
  styleUrls: ['./control-input.component.scss']
})
export class ControlInputComponent implements OnInit {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Output('blurAction') _blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') _enterKey: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

}


