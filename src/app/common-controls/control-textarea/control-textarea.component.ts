import { SpecialFormControl } from '../../configurations/specialForms/SpecialForms';
import { Component, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'control-text-area',
  templateUrl: './control-textarea.component.html',
  styleUrls: ['./control-textarea.component.scss'],
})
export class ControlTextAreaComponent{
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Output('blurAction') _blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') _enterKey: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
}