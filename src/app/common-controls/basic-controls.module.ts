import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { ControlInputComponent } from './control-input/control-input.component';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { ControlCheckboxComponent } from './control-checkbox/control-checkbox.component';
import { ControlSelectComponent } from './control-select/control-select.component';
import { PipesModule } from '../configurations/pipes/pipes-module.module';
import { ControlAutocompleteComponent } from './control-autocomplete/control-autocomplete.component';
import { ControlDatepickerComponent } from './control-datepicker/control-datepicker.component';
import { ControlTextAreaComponent } from './control-textarea/control-textarea.component';

const COMPONENTS = [
  ControlAutocompleteComponent,
  ControlCheckboxComponent,
  ControlDatepickerComponent,
  // ControlMaskedInputComponent,
  // ControlKeyfilterComponent,
  ControlTextAreaComponent,
  // ControlTimepickerComponent,
  // ControlPhonesComponent,
  // ControlUploadComponent,
  ControlSelectComponent,
  ControlInputComponent
]


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CustomMaterialModule,
    PipesModule
  ],
  declarations: [
    ...COMPONENTS,
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class BasicControlsModule { }
