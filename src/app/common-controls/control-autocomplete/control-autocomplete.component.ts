import { Component, Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { SpecialFormControl } from '../../configurations/specialForms/SpecialForms';
import { FormControl } from '@angular/forms';
import { debounceTime, map, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MatAutocompleteSelectedEvent } from '@angular/material';


@Component({
  selector: 'control-autocomplete',
  templateUrl: './control-autocomplete.component.html',
  styleUrls: ['./control-autocomplete.component.scss'],
})
export class ControlAutocompleteComponent implements AfterContentInit {
  @Input('sfcontrol') _sfcontrol: SpecialFormControl;
  @Output('blurAction') _blurAction: EventEmitter<any> = new EventEmitter();
  @Output('enterKey') _enterKey: EventEmitter<any> = new EventEmitter();

  myControl = new FormControl();
  filteredOptions: string[];
  constructor(private _http: HttpClient) { }

  ngAfterContentInit() {
    if (this._sfcontrol)
      this.myControl.valueChanges.pipe(
        debounceTime(1000),
        map(value => this._sfcontrol.data[0](value)),
        switchMap(value => this.get(value)),
        map(value => this._sfcontrol.data[1](value))
      ).subscribe((data) => {
        this.filteredOptions = data;
      });
  }

  get(url) {
    return this._http.get(url);
  }
  configValue(data) {
    return data && data.name ? data.name : ''
  }
  optionSelected(data: MatAutocompleteSelectedEvent) {
    this._sfcontrol.setValue(data.option.value.id);
  }
}
