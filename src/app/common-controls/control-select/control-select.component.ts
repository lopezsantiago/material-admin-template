import { SpecialFormControl } from '../../configurations/specialForms/SpecialForms';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'control-select',
  templateUrl: './control-select.component.html',
  styleUrls: ['./control-select.component.scss']
})
export class ControlSelectComponent {
  @Input('readOnly') _readOnly = false;
  @Input('sfcontrol') sfcontrol: SpecialFormControl;
  @Output('sendObject') _sendObject: EventEmitter<Object> = new EventEmitter();

  value: any = "";

  ngOnInit() { }

  selectItem(item) {
    this.sfcontrol.setValue(item);
    this._sendObject.emit(item);
  }

}
