import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'control-upload',
  templateUrl: './control-upload.component.html',
  styleUrls: ['./control-upload.component.scss']
})
export class ControlUploadComponent implements OnInit {
  @Input('fileTypes') _fileTypes = 'image/png, .jpeg, .jpg, image/gif, .pdf, .mp4 , .mp3';
  @Input('readOnly') _readOnly = false;
  _file: any;
  _reloadInput = true;
  // msgs: any[];

  uploadedFiles: any[] = [];
  constructor(
  ) { }

  //metodo para reiniciar los archivos.

  reloadInputFile() {
    this._reloadInput = false;
    setTimeout(() => {
      this._reloadInput = true;
    }, 100);
  }

  onUpload(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }
  }


  ngOnInit() {
  }

  resetFiles() {
    this._file = null;
    this.reloadInputFile();
  }

  deleteFile() {
    if (this._file) {
      this._file = null;
      this.reloadInputFile()
    }
  }

  saveFile(evento) {
    let auxFile = evento.srcElement.files[0];
    if (auxFile && auxFile.size < 1.25e+7) {
      this._file = auxFile;
    } else
      this.reloadInputFile();
  }

}
