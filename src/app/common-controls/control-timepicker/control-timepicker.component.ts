import { FormControl, NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor } from '@angular/forms';
import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { SpecialFormControl } from '../../configurations/specialForms/SpecialForms';

@Component({
  selector: 'control-timepicker',
  templateUrl: './control-timepicker.component.html',
  styleUrls: ['./control-timepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ControlTimepickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ControlTimepickerComponent),
      multi: true,
    }]
})

export class ControlTimepickerComponent implements OnInit, ControlValueAccessor {
  @Input('title') _title: string = '';
  @Input('readOnly') _readonly = false;
  @Input('sfcontrol') private _sfcontrol: SpecialFormControl;

  reloadFlag = true;
  private _value: any;
  private id: string = idGenerado('time_input');
  constructor() { }

  ngOnInit() {

  }

  dateChange() {
    let auxDate = this._sfcontrol.value.split('T')[0];
    if (this._value) {
      this._sfcontrol.setValue(auxDate + 'T' + this._value + ':00.000Z');
    } else {
      this._sfcontrol.setValue(auxDate + 'T' + '00:00:00.000Z');
    }
  }

  ngOnDestroy() {

  }

  writeValue(val: any) {
    if (val) {
      this._value = val.split('T')[1].slice(0,5);
    } else {
      this._value = '00:00';
      this.reloadFlag = false;
      setTimeout(() => {
        this.reloadFlag = true;
      }, 10)
    }
  }

  // registers 'fn' that will be fired wheb changes are made
  // this is how we emit the changes back to the form
  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  // validates the form, returns null when valid else the validation object
  validate(c: FormControl) {
    return null;
  }

  // not used, used for touch input
  registerOnTouched() { }

  private propagateChange = (_: any) => { };
}

let i = 0;
function idGenerado(prefix: string): string {
  return prefix + ++i;
}


