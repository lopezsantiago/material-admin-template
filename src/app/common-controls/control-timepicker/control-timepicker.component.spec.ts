import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTimepickerComponent } from './control-timepicker.component';

describe('ControlTimepickerComponent', () => {
  let component: ControlTimepickerComponent;
  let fixture: ComponentFixture<ControlTimepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTimepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTimepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
