import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../configurations/ngRx/reducers/app.states';
import * as HomeActions from '../configurations/ngRx/actions/home.actions';
import { requestDataModel } from '../configurations/models/requestData.model';
import { HomeGeneralSelectors } from '../configurations/ngRx/reducers/home.reducer';
import { FoodCategories } from '../configurations/basic masters/food-categories';
import { FoodGroups } from '../configurations/basic masters/food-groups';
import { masterTypes } from '../configurations/basic masters/masterTypes';
import { measureUnits } from '../configurations/basic masters/measure-units';
import { FoodModel } from '../configurations/models/food.model';

@Injectable({
  providedIn: 'root'
})
export class HomeFacadeService {
  users$ = this.store.select(HomeGeneralSelectors.UsersSelector);
  foods$ = this.store.select(HomeGeneralSelectors.FoodSelector);
  foodCategories$ = this.store.select(HomeGeneralSelectors.FoodCategoriesSelector);
  foodGroups$ = this.store.select(HomeGeneralSelectors.FoodGroupsSelector);
  measureUnits$ = this.store.select(HomeGeneralSelectors.MeasureUnitsSelector);

  constructor(private store: Store<AppState>) { }

  getMultpleMastersByCode(masterTypes: string[]) {
    masterTypes.map((type) => {
      this.getMasterByCode(type);
    })
  }

  getMasterByCode(masterType) {
    switch (masterType) {
      case masterTypes.users:
         this.getAllUsers();break;
        case masterTypes.foods:
         this.getAllfoods(); break;
      case masterTypes.foodCategories:
         this.getAllfoodCategories();break;
      case masterTypes.foodGroups:
         this.getAllfoodGroups();break;
      case masterTypes.measureUnits:
         this.getAllMeasureUnits();break;
    }
  }

  getAllUsers() {
    const requestData: requestDataModel = {
      uri: 'users',
      success: HomeActions.GetAllUsersAction
    };
    this.store.dispatch(new HomeActions.GetRequestAction(requestData));
  }

  getAllfoods() {
    // const requestData: requestDataModel = {
    //   uri: 'users',
    //   success: HomeActions.GetAllFoodCategoriesAction
    // };
    const foodsMock:FoodModel[]=[
      {
        calories:23,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'pollo',
        quantity:1
      },
      {
        calories:16,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'carne',
        quantity:1
      },
      {
        calories:14,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'pezcado',
        quantity:1
      },      {
        calories:12,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'higado',
        quantity:1
      }
      ,      {
        calories:11,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'chicharron',
        quantity:1
      }
      ,      {
        calories:1,
        categorie:{name:'Proteina'},
        group:{name:'grasa'},
        unity:{name:'kilos'},
        name:'tocino',
        quantity:1
      }
    ]
    this.store.dispatch(new HomeActions.GetAllFoodsAction(foodsMock));
  }

  getAllfoodCategories() {
    // const requestData: requestDataModel = {
    //   uri: 'users',
    //   success: HomeActions.GetAllFoodCategoriesAction
    // };
    this.store.dispatch(new HomeActions.GetAllFoodCategoriesAction(FoodCategories));
  }

  getAllfoodGroups() {
    // const requestData: requestDataModel = {
    //   uri: 'users',
    //   success: HomeActions.GetAllFoodGroupsAction
    // };
    this.store.dispatch(new HomeActions.GetAllFoodGroupsAction(FoodGroups));
  }

  getAllMeasureUnits() {
    // const requestData: requestDataModel = {
    //   uri: 'users',
    //   success: HomeActions.GetAllMeasureUnitsAction
    // };
    this.store.dispatch(new HomeActions.GetAllMeasureUnitsAction(measureUnits));
  }

  createUser(user) {
    const requestData: requestDataModel = {
      uri: 'users',
      body: user,
      success: HomeActions.CreateUsersAction
    };
    this.store.dispatch(new HomeActions.CreateRequestAction(requestData));
  }

}
