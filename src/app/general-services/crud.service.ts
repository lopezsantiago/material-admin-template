import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { localIp } from "../configurations/General-configs";
import { Observable } from "rxjs";

@Injectable()
export class CrudService {
  private _headers: HttpHeaders;
  private _ip: string;

  constructor(private _http: HttpClient) {
    this._ip = localIp;
    this.initHeaders();
  }

  initHeaders() {
    this._headers = new HttpHeaders();
    this._headers = this._headers.set("Content-Type", "application/json");
  }

  get(uri: string): Observable<any> {
    let rxOptions: Object = {};
    this.addAuthorizationHeader();
    rxOptions["headers"] = this._headers;
    return this._http.get(this._ip + "/" + uri, rxOptions);
  }

  post(uri: string, body: Object) {
    let rxOptions: Object = {};
    this.addAuthorizationHeader();
    rxOptions["headers"] = this._headers;
    return this._http.post(this._ip + "/" + uri, body, rxOptions);
  }

  put(uri: string, body: Object) {
    let rxOptions: Object = {};
    this.addAuthorizationHeader();
    rxOptions["headers"] = this._headers;
    return this._http.put(this._ip + "/" + uri, body, rxOptions);
  }

  postLogin(uri: string, body: string) {
    let rxOptions: Object = {};
    this.initHeaders();
    rxOptions["headers"] = this._headers;
    return this._http.post(this._ip + "/" + uri, body, rxOptions);
  }

  delete(uri: string, params?: Object) {
    let rxOptions: Object = {};
    rxOptions["params"] = params;
    // body ? rxOptions['body'] = body : 0;
    this.addAuthorizationHeader();
    rxOptions["headers"] = this._headers;
    return this._http.delete(this._ip + "/" + uri, rxOptions);
  }

  private addAuthorizationHeader(): void {
    if (!this._headers.has("ap-tk-sc")) {
    }
  }
}
