import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../configurations/ngRx/reducers/app.states';
import * as AuthActions from '../configurations/ngRx/actions/auth.actions';

import { LoginModel } from '../configurations/models/login.model';

@Injectable({
  providedIn: 'root'
})
export class AuthFacadeService {

  constructor(private store: Store<AppState>, ) { }

  doLogin(credentials: LoginModel) {
    this.store.dispatch(new AuthActions.doLoginAction(credentials))
  }


}
