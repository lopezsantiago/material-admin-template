import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { HomeFacadeService } from 'src/app/general-services/home-facade.service';
import { masterTypes } from 'src/app/configurations/basic masters/masterTypes';

@Injectable()
export class ResolverService implements Resolve<any>  {

  constructor(private _homeFacade: HomeFacadeService) { }

  resolve(route: ActivatedRouteSnapshot) {
    switch (route.routeConfig.path) {
      case 'foods':
        this._homeFacade.getMultpleMastersByCode([
          masterTypes.foodCategories,
          masterTypes.foodGroups,
          masterTypes.measureUnits,
          masterTypes.foods
        ]);
    }
  }
}
